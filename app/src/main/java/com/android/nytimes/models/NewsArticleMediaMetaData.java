package com.android.nytimes.models;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class NewsArticleMediaMetaData implements Serializable {
    @SerializedName("url")
    private String url;

    public String getUrl() {
        return url;
    }
}
