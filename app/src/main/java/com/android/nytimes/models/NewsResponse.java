package com.android.nytimes.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class NewsResponse {

    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("num_results")
    @Expose
    private Integer num_results;
    @SerializedName("results")
    @Expose
    private List<NewsArticle> newsList = null;

    public List<NewsArticle> getNewsList() {
        return newsList;
    }

}