package com.android.nytimes.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class NewsArticleMedia implements Serializable {

    public List<NewsArticleMediaMetaData> getNewsArticleMediaMetaDataList() {
        return newsArticleMediaMetaDataList;
    }

    @SerializedName("media-metadata")
    @Expose
    private List<NewsArticleMediaMetaData> newsArticleMediaMetaDataList = null;


}
