package com.android.nytimes.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class NewsArticle implements Serializable {

    public static final String NEWS_ARTICLE_KEY = "NEWS_ARTICLE_KEY";

    @SerializedName("url")
    private String url;
    @SerializedName("byline")
    private String byline;
    @SerializedName("title")
    private String title;
    @SerializedName("published_date")
    private String published_date;
    @SerializedName("abstract")
    private String abstractarticle;

    public List<NewsArticleMedia> getNewsArticleMediaList() {
        return newsArticleMediaList;
    }

    @SerializedName("media")
    @Expose
    private List<NewsArticleMedia> newsArticleMediaList = null;

    public String getAbstractarticle() {
        return abstractarticle;
    }

    public String getByline() {
        return byline;
    }

    public String getTitle() {
        return title;
    }

    public String getPublished_date() {
        return published_date;
    }

}