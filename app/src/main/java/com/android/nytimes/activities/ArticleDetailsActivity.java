package com.android.nytimes.activities;

import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.nytimes.R;
import com.android.nytimes.models.NewsArticle;
import com.bumptech.glide.Glide;

import androidx.appcompat.app.AppCompatActivity;

public class ArticleDetailsActivity extends AppCompatActivity {

    TextView tvArticleDetailTitle, tvArticleDetailAbstract, tvArticleDetailDate;
    ImageView ivArticleDetail;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_article_details);

        findViews();

        getIntentExtras();

    }

    private void getIntentExtras() {

        NewsArticle newsArticle = (NewsArticle) getIntent().getSerializableExtra(NewsArticle.NEWS_ARTICLE_KEY);
        if (newsArticle != null) {

            tvArticleDetailTitle.setText(newsArticle.getTitle());
            tvArticleDetailAbstract.setText(newsArticle.getAbstractarticle());
            tvArticleDetailDate.setText(newsArticle.getPublished_date());

            Glide.with(this)
                    .load(newsArticle.getNewsArticleMediaList().get(0).getNewsArticleMediaMetaDataList().get(0).getUrl())
                    .into(ivArticleDetail);
        } else {

            Toast.makeText(this, R.string.could_not_load_article, Toast.LENGTH_LONG).show();
        }
    }

    private void findViews() {
        tvArticleDetailTitle = findViewById(R.id.tvArticleDetailTitle);
        tvArticleDetailAbstract = findViewById(R.id.tvArticleDetailAbstract);
        tvArticleDetailDate = findViewById(R.id.tvArticleDetailDate);
        ivArticleDetail = findViewById(R.id.ivArticleDetail);
    }
}
