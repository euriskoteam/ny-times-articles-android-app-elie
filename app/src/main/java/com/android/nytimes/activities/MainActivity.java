package com.android.nytimes.activities;

import android.app.Activity;
import android.os.Bundle;
import android.widget.Toast;

import com.android.nytimes.R;
import com.android.nytimes.adapters.NewsAdapter;
import com.android.nytimes.models.NewsArticle;
import com.android.nytimes.models.NewsResponse;
import com.android.nytimes.utils.NetworkUtils;
import com.android.nytimes.viewmodels.NewsViewModel;

import java.util.ArrayList;
import java.util.List;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

public class MainActivity extends AppCompatActivity {
    ArrayList<NewsArticle> articleArrayList = new ArrayList<>();
    NewsAdapter newsAdapter;
    RecyclerView rvArticles;
    NewsViewModel newsViewModel;
    Activity activity;
    SwipeRefreshLayout srlArticles;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        findViews();

        initialize();

        setListeners();

        getNewsArticles();

    }

    private void findViews() {
        rvArticles = findViewById(R.id.rvArticles);
        srlArticles = findViewById(R.id.srlArticles);
    }

    private void setListeners() {
        srlArticles.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getNewsArticles();
            }
        });
    }

    private void initialize() {
        activity = this;
        newsViewModel = ViewModelProviders.of(this).get(NewsViewModel.class);
        newsViewModel.init();
    }

    private void getNewsArticles() {

        if (NetworkUtils.isConnectionAvailable(activity)) {
            srlArticles.setRefreshing(true);
            newsViewModel.requestNews();
            newsViewModel.getNewsRepository().observe(this, new Observer<NewsResponse>() {
                @Override
                public void onChanged(NewsResponse newsResponse) {
                    try {
                        List<NewsArticle> newsArticles = newsResponse.getNewsList();
                        articleArrayList.addAll(newsArticles);
                        newsAdapter.notifyDataSetChanged();
                    } catch (Exception ex) {
                        Toast.makeText(activity, R.string.something_wrong, Toast.LENGTH_LONG).show();
                    } finally {
                        srlArticles.setRefreshing(false);
                    }
                }
            });

            setupRecyclerView();
        } else {
            Toast.makeText(activity, R.string.check_internet_connection, Toast.LENGTH_LONG).show();
            srlArticles.setRefreshing(false);
        }
    }

    private void setupRecyclerView() {
        if (newsAdapter == null) {
            newsAdapter = new NewsAdapter(activity, articleArrayList);
            rvArticles.setAdapter(newsAdapter);
        } else {
            newsAdapter.notifyDataSetChanged();
        }
    }
}
