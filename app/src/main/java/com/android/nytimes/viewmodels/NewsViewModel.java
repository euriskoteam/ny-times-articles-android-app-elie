package com.android.nytimes.viewmodels;

import com.android.nytimes.models.NewsResponse;
import com.android.nytimes.network.NewsRepository;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

public class NewsViewModel extends ViewModel {

    private MutableLiveData<NewsResponse> mutableLiveData;
    private NewsRepository newsRepository;

    public void init(){
        if (mutableLiveData != null){
            return;
        }
        newsRepository = NewsRepository.getInstance();

    }

    public void requestNews(){
        mutableLiveData = newsRepository.getNews();
    }

    public LiveData<NewsResponse> getNewsRepository() {
        return mutableLiveData;
    }

}