package com.android.nytimes.adapters;


import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.android.nytimes.R;
import com.android.nytimes.activities.ArticleDetailsActivity;
import com.android.nytimes.models.NewsArticle;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.Target;
import com.mikhaellopez.circularimageview.CircularImageView;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

public class NewsAdapter extends RecyclerView.Adapter<NewsAdapter.NewsViewHolder> {

    Context mCtx;
    List<NewsArticle> newsArticleList;

    public NewsAdapter(Context mCtx, List<NewsArticle> newsArticleList) {
        this.mCtx = mCtx;
        this.newsArticleList = newsArticleList;
    }

    @NonNull
    @Override
    public NewsViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mCtx).inflate(R.layout.article_list_item, parent, false);
        return new NewsViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull NewsViewHolder holder, int position) {
        NewsArticle news = newsArticleList.get(position);

        Glide.with(mCtx)
                .load(news.getNewsArticleMediaList().get(0).getNewsArticleMediaMetaDataList().get(0).getUrl())
                .apply(
                        new RequestOptions()
                                .error(R.drawable.imagenotfound)
                                .centerCrop()
                )
                .listener(new RequestListener<Drawable>() {
                    @Override
                    public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                        //on load failed
                        return false;
                    }

                    @Override
                    public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                        //on load success
                        return false;
                    }
                })
                .into(holder.ivArticle);

        holder.tvArticleTitle.setText(news.getTitle());
        holder.tvArticleAuthor.setText(news.getByline());
        holder.tvArticleDate.setText(news.getPublished_date());
    }

    @Override
    public int getItemCount() {
        return newsArticleList.size();
    }

    class NewsViewHolder extends RecyclerView.ViewHolder {

        CircularImageView ivArticle;
        TextView tvArticleTitle, tvArticleAuthor, tvArticleDate;
        CardView cvArticleItem;


        public NewsViewHolder(View itemView) {
            super(itemView);

            findViews(itemView);
            setListeners();
        }

        private void findViews(View itemView) {

            ivArticle = itemView.findViewById(R.id.ivArticle);
            tvArticleTitle = itemView.findViewById(R.id.tvArticleTitle);
            tvArticleAuthor = itemView.findViewById(R.id.tvArticleAuthor);
            tvArticleDate = itemView.findViewById(R.id.tvArticleDate);
            cvArticleItem = itemView.findViewById(R.id.cvArticleItem);

        }

        private void setListeners() {

            cvArticleItem.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    openArticleDetails();

                }
            });

        }

        private void openArticleDetails() {
            Intent intent = new Intent(mCtx, ArticleDetailsActivity.class);
            NewsArticle newsArticle = newsArticleList.get(getAdapterPosition());
            intent.putExtra(NewsArticle.NEWS_ARTICLE_KEY, newsArticle);
            mCtx.startActivity(intent);
        }


    }
}