package com.android.nytimes.network;

import com.android.nytimes.models.NewsResponse;
import com.android.nytimes.utils.APIVars;

import retrofit2.Call;
import retrofit2.http.GET;

public interface  NewsApi {

        @GET(APIVars.NEWS_API_END_POINT)
        Call<NewsResponse> getNewsList();

}
